import express from 'express';
import * as productos from './productos'

const app = express();
app.use(express.json());


app.get('/', function(request, response){
    response.send('¡Bienvenidos a Express!');
})

app.get('/productos', function(request, response){
    response.send(productos.getStock());
})

app.post('/productos', function(request, response) {
    const body=request.body;
    productos.storeProductos(body)
    response.send("Producto agregado")
})

app.delete('/productos/:id' , function(request, response) {
    const idProducto=Number(request.params.id);
    productos.deleteProductos(idProducto) 
    response.send( "Producto "+ idProducto +" eliminado")
}) 

/*app.put('/productos/:id' , function(request, response) {
    const idProducto=Number(request.params.id);
    const body=request.body;
    productos.updateProductos(idProducto, body) 
    response.send( "Producto "+ idProducto +" actualizado")
})*/


app.listen(3000, function(){
    console.info("Servidor Escuchando en htttp://localhost:3000")
})